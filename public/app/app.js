angular.module('wootingProfiles', [
                                   'authServices',
                                   'codeControllers',
                                   'codeServices',
                                   'mainController',
                                   'ngAnimate',
                                   'ngclipboard',
                                   'userControllers',
                                   'userServices',
                                   'wootingProfilesRoutes',
                                   'gameControllers',
                                 ])

.config(function($httpProvider) {
  $httpProvider.interceptors.push('AuthInterceptors');
});
