angular.module('codeServices', [])

.factory("Code", function($http, $routeParams) {
  codeFactory = {};

  codeFactory.sendMailToAdmin = function(data) {
    return $http.post('/api/sendmailtoadmin', data);
  }

  codeFactory.getCodes = function() {
      return $http.get("/api/codes").
          then(function(response) {
              return response;
          }, function(response) {
              alert("Error finding profile codes.");
          });
  }

  codeFactory.getGames = function() {
      return $http.get("/api/games").
          then(function(response) {
              return response;
          }, function(response) {
              alert("Error finding games.");
          });
  }

  codeFactory.queryIgdb = function(query) {    
    return $http.get("/api/queryigdb/" + query).
      then(function(response) {
        return response;
      }, function(response) {
        alert("Error querying IGDB.");
      });
  }

  codeFactory.getGameDetails = function(id) {
    return $http.get("/api/getgamedetails/" + id).
        then(function(response) {
            return response;
        }, function(response) {
            alert("Error querying IGDB.");
        });
  }

  codeFactory.checkGame = function(id) {
    return $http.get("/api/checkforgame/" + id).
        then(function(response) {
            return response;
        }, function(response) {
            alert("Error querying MongoDB.");
        });
  }

  codeFactory.createGame = function(data) {
    return $http.post('/api/games', data);
  }

  codeFactory.appendCode = function(data) {
    return $http.post('/api/addcodetogame', data);
  }

  codeFactory.create = function(codeData) {
    return $http.post('/api/codes', codeData);
  }

  codeFactory.updateUserCodes = function(data) {
    return $http.post('/api/updateusercodes', data);
  }

  codeFactory.getGame = function(id) {
    return $http.get("/api/games/" + id).
        then(function(response) {
            return response;
        }, function(response) {
            alert("Error querying MongoDB.");
        });
  }

  codeFactory.updateCode = function(data) {
    return $http.post("/api/updategamecode", data);
  }

  codeFactory.getWootingObj = function(code) {
    return $http.get("/api/wooting/" + code).
        then(function(response) {
            return response;
        }, function(response) {
            alert("Error querying the Wooting API.");
        });
  }

  return codeFactory
});
