var FacebookStrategy = require('passport-facebook').Strategy;
var TwitterStrategy = require('passport-twitter').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var user = require('../models/user');
var session = require('express-session');
var User = require('../models/user');
var jwt = require('jsonwebtoken');
var express_secret = process.env.EXPRESS_SECRET || 'keyboardcat';
var jwt_secret = process.env.JWT_SECRET || 'PizzaPartyPimps';

module.exports = function(app, passport){

  app.use(passport.initialize());
  app.use(passport.session());
  app.use(session({ secret: express_secret, resave: false, saveUninitialized: true, cookie: { secure: false } }));

  passport.serializeUser(function(user, done) {
    token = jwt.sign({ id: user.id, username: user.username, email: user.email, password: user.password }, jwt_secret, { expiresIn: '72h' });
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });

  // passport.use(new FacebookStrategy({
  //     clientID: process.env.FB_CLIENTID,
  //     clientSecret: process.env.FB_CLIENTSECRET,
  //     callbackURL: process.env.FB_CALLBACKURL,
  //     profileFields: ['id', 'displayName', 'email']
  //   },
  //   function(accessToken, refreshToken, profile, done) {
  //     User.findOne({ email: profile._json.email }).select('username password email id codes admin').exec(function(err, user) {
  //       if (err) done(err);
  //
  //       if(user && user != null) {
  //         done(null, user);
  //       } else {
  //         done(err);
  //       }
  //     });
  //   }
  // ));
  //
  // passport.use(new TwitterStrategy({
  //     consumerKey: process.env.TWITTER_CONSUMERKEY,
  //     consumerSecret: process.env.TWITTER_CONSUMERSECRET,
  //     callbackURL: process.env.TWITTER_CALLBACKURL,
  //     userProfileURL: "https://api.twitter.com/1.1/account/verify_credentials.json?include_email=true"
  //   },
  //   function(token, tokenSecret, profile, done) {
  //     User.findOne({ email: profile.emails[0].value }).select('username password email id codes admin').exec(function(err, user) {
  //       if (err) done(err);
  //
  //       if(user && user != null) {
  //         done(null, user);
  //       } else {
  //         done(err);
  //       }
  //     });
  //   }
  // ));
  //
  // // Use the GoogleStrategy within Passport.
  // //   Strategies in Passport require a `verify` function, which accept
  // //   credentials (in this case, an accessToken, refreshToken, and Google
  // //   profile), and invoke a callback with a user object.
  // passport.use(new GoogleStrategy({
  //     clientID: process.env.GOOGLE_CLIENTID,
  //     clientSecret: process.env.GOOGLE_CLIENTSECRET,
  //     callbackURL: process.env.GOOGLE_CALLBACKURL
  //   },
  //   function(accessToken, refreshToken, profile, done) {
  //     User.findOne({ email: profile.emails[0].value }).select('username password email id codes admin').exec(function(err, user) {
  //       if (err) done(err);
  //
  //       if(user && user != null) {
  //         done(null, user);
  //       } else {
  //         done(err);
  //       }
  //     });
  //   }
  // ));
  //
  //   // GET /auth/google
  // //   Use passport.authenticate() as route middleware to authenticate the
  // //   request.  The first step in Google authentication will involve
  // //   redirecting the user to google.com.  After authorization, Google
  // //   will redirect the user back to this application at /auth/google/callback
  // app.get('/auth/google', passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/plus.login', 'profile', 'email'] }));
  //
  // // GET /auth/google/callback
  // //   Use passport.authenticate() as route middleware to authenticate the
  // //   request.  If authentication fails, the user will be redirected back to the
  // //   login page.  Otherwise, the primary route function function will be called,
  // //   which, in this example, will redirect the user to the home page.
  // app.get('/auth/google/callback', passport.authenticate('google', { failureRedirect: '/googleerror' }), function(req, res) {
  //   res.redirect('/google/' + token);
  // });
  //
  // // Routes for Twitter Auth
  // app.get('/auth/twitter', passport.authenticate('twitter'));
  //
  // app.get('/auth/twitter/callback', passport.authenticate('twitter', { failureRedirect: '/twittererror' }), function(req, res) {
  //   res.redirect('/twitter/' + token);
  // });
  //
  // // Routes for Facebook Auth
  // app.get('/auth/facebook', passport.authenticate('facebook', { scope: 'email' }));
  //
  // app.get('/auth/facebook/callback', passport.authenticate('facebook', {failureRedirect: '/facebookerror' }), function(req, res) {
  //   res.redirect('/facebook/' + token);
  // });

  return passport;
};
