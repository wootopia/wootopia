var User = require('../models/user');
var Code = require('../models/code');
var Game = require('../models/game');
var jwt = require('jsonwebtoken');
var request = require('request');
var jwt_secret = process.env.JWT_SECRET || 'PizzaPartyPimps';

module.exports = function(router) {

  // User Registratio Route
  // Endpoint: /api/users
  router.post('/users', function(req, res) {
    var user = new User();
    user.username = req.body.username;
    user.password = req.body.password;
    user.email = req.body.email;
    if (!req.body.username || !req.body.password || !req.body.email) {
      res.json({ success: false, message: "Ensure username, email and password are provided correctly." });
    } else  {
      user.save(function(err) {
        if (err) {
          if (err.errors != null) {
            if (err.errors.username) {
              res.json({ success: false, message: err.errors.username.message});
            } else if (err.errors.email) {
              res.json({ success: false, message: err.errors.email.message});
            } else if (err.errors.password) {
              res.json({ success: false, message: err.errors.password.message});
            } else {
              res.json({ success: false, message: err });
            }
          } else if (err) {
            if (err.code == 11000) {
              res.json({ success: false, message: 'Email of username is already taken' });
              // if (err.errormsg[61] == "u") {
              //   res.json({ success: false, message: 'That username already exists' });
              // } else if (err.errormsg[61] == "e") {
              //   res.json({ success: false, message: 'That email adress is already taken' });
              // }
            } else {
              res.json({ success: false, message: err });
            }
          }
        } else {
          res.json({ success: true, message: "user created!"});
        }
      });
    }
  });

  router.get('/users', function(req, res) {
    User.find(function (err, results) {
      if (err) {
        handleError(res, err.message, "Failed to get users");
      } else {
        res.status(200).json(results);
      }
    });
  });

  router.post('/updateusercodes', function(req, res) {
    User.update({ _id: req.body.userdata.id }, {"$push": {codes: {"code": req.body.newcode }}}).exec(function(err, user) {
      if (err) throw err;
      res.json(user);
    });
  });


  router.post('/codes', function(req, res) {
    var code = new Code();
    code.code = req.body.code;
    code.kb_model = req.body.kb_model;
    code.game = req.body.game;
    code.notes = req.body.notes;
    code.save(function(err) {
      if (err) {
        if (err.errors != null) {
          if (err.errors.code) {
            res.json({ success: false, message: err.errors.code.message });
          }
        } else if (err) {
          if (err.code == 11000) {
            res.json({ success: false, message: 'Code is already in the Wooting Profiles database' });
          } else {
            res.json({ success: false, message: err });
          }
        }
      } else {
        res.json({ success: true, message: "code submitted!"});
      }
    });
  });

  router.get('/codes', function(req, res) {
    Code.find(function (err, results) {
      if (err) {
        handleError(res, err.message, "Failed to get codes");
      } else {
        res.status(200).json(results);
      }
    });
  });

  router.post('/games', function(req, res) {
    var game = new Game();
    game.igdb_id = req.body.id;
    game.name = req.body.name;
    game.hasPublishedCode = false;
    game.cover = req.body.cover;
    game.screenshots = req.body.screenshots;
    game.codes = { "code": req.body.code, "published": false, "notes": req.body.code_notes, "rgb": req.body.code_rgb }
    game.save(function(err) {
      if (err) {
        if (err.code == 11000) {
          res.json({ duplicate: true, success: false, message: 'Game is already in the Wooting Profiles database' });
        } else {
          res.json({ duplicate: false, success: false, message: err });
        }
      } else {
        res.json({ duplicate: false, success: true, message: "Game added to database!"});
      }
    });
  });

  router.get('/games', function(req, res) {
    Game.find(function (err, results) {
      if (err) {
        handleError(res, err.message, "Failed to get games");
      } else {
        res.status(200).json(results);
      }
    });
  });

  router.get('/games/:id', function(req, res) {
    Game.findOne({ igdb_id: req.params.id }).exec(function(err, game) {
      if (err) throw err;
      res.status(200).json(game);
    });
  });

  router.get('/games/:id/:code', function(req, res) {
    Game.findOne({ igdb_id: req.params.id, 'codes.code': req.params.code }, { 'codes.code.$': req.params.code  }).exec(function(err, game) {
      if (err) throw err;

      res.status(200).json(game);
    });
  });

  router.post('/addcodetogame', function(req, res) {
    Game.update({ igdb_id: req.body.id }, {"$push": {codes: {"code": req.body.code, "published": false, "notes": req.body.code_notes, "rgb": req.body.code_rgb}}}).exec(function(err, game) {
      if (err) throw err;
      res.json(game);
    });
  });

  router.post('/updategamecode', function(req, res) {

    if (req.body.action == "submit") {
      Game.update({_id: req.body.game_id, 'codes._id': req.body.code_id},
        {'$set': {
          'codes.$.published': true,
          'hasPublishedCode': true
        }},
        function(err, results) {
          if (err) throw err;
          res.status(200).json(results);
      });
    } else {
      Game.update({ _id: req.body.game_id }, { "$pull": { "codes": { "_id": req.body.code_id } }}, { safe: true, multi:true }, function(err, results) {
        if (err) throw err;
        res.status(200).json(results);
      });
    }

  });

  router.get('/checkforgame/:id', function(req, res) {
    Game.findOne({ igdb_id: req.params.id }).exec(function(err, game) {
      if (err) throw err;

      if (!game) {
        res.send(false);
      } else {
        res.send(true);
      }
    });
  });

  // User Login Route
  // Endpoint: /api/authenticate
  router.post('/authenticate', function(req, res) {
    User.findOne({ username: req.body.username }).select('email username password id codes admin').exec(function(err, user) {
      if (err) throw err;

      if (!user) {
        if (req.body.password) {
          res.json({ success: false, "message": "User could not be authenticated" });
        } else {
          res.json({ success: false, message: "No password was provided." });
        }
      } else if (user) {
         var validPassword = user.comparePassword(req.body.password);
         if (!validPassword) {
           res.json({ success: false, message: "Could not authenticate password" });
         } else {
           var token = jwt.sign({ username: user.username, email: user.email, password: user.password, admin: user.admin, codes: user.codes }, jwt_secret, { expiresIn: '72h' });
           res.json({ success: true, message: "User was authenticated", token: token });
         }
      }
    });
  });

  router.get('/queryigdb/:query', function(req, res) {
    var options = {
      method: 'GET',
      url: 'https://api-v3.igdb.com/games',
      headers: {
        'user-key': process.env.IGDB_USER_KEY
      },
      body: 'search ' + JSON.stringify(req.params.query) +'; fields name;'
    };

    request(options, function (error, response, body) {
      console.log(body);
      
      if (error) throw new Error(error);
    }).pipe(res);
  });

  router.get('/getgamedetails/:id', function(req, res) {
    var options = {
      method: 'GET',
      url: 'https://api-v3.igdb.com/games',
      headers: {
        'user-key': process.env.IGDB_USER_KEY
      },
      body: 'fields name,cover,first_release_date,screenshots; where id = ' + req.params.id + ';' 
    };

    request(options, function (error, response, body) {
      if (error) throw new Error(error);
    }).pipe(res);
  });

  router.post('/sendmailtoadmin', function(req, res) {
    const options = { method: 'POST',
      url: 'https://api.sendgrid.com/v3/mail/send',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + process.env.SENDGRID_API_KEY
      },
      body: {
        personalizations: [
          {
            to: [
              {
                email: 'sander@wootingprofiles.com',
                name: 'SuperSchek'
              }
            ],
            subject: 'Code submitted for ' + req.body.game + ' | Wootopia'
          }
        ],
        from: {
          email: 'admin@wootingprofiles.com',
          name: 'Wootopia AdminBot'
        },
        content: [
          {
            type: 'text/html',
            value: 'Someone submitted a code. It\'s waiting for approval.<br><br>Game: ' + req.body.game + '<br>Code: ' + req.body.code + '<br>Notes: ' + req.body.notes + '<br><br><a href="http:wootingprofiles.com/admin">Admin panel</a>'
          }
        ]
      },
      json: true
    };

    request(options, (error, response, body) => {
      if (error) throw new Error(error);
    }).pipe(res);
  });

  router.get('/wooting/:code', function(req, res) {
    request({
      uri: 'https://le8vj2dxtg.execute-api.us-west-2.amazonaws.com/dev/wooting-profiles/' + req.params.code
    }).pipe(res);
  });

  router.use(function(req, res, next) {
    var token = req.body.token || req.body.query || req.headers['x-access-token'];

    if (token) {
      jwt.verify(token, jwt_secret, function(err, decoded) {
        if (err) {
          res.json({ success: false, message: "Token invalid" });
        } else {
          req.decoded = decoded;
          next();
        }
      });
    } else {
      res.json({ success: false, message: "No token provided" });
    }
  });
  router.post('/me', function(req, res) {
    res.send(req.decoded);
  });

  return router;
}
