angular.module('authServices', [])

.factory('Auth', function($http, AuthToken) {
  authFactory = {};

  // User.create(loginData);
  authFactory.login = function(loginData) {
    return $http.post('/api/authenticate', loginData).then(function(data) {
      AuthToken.setToken(data.data.token);
      return data;
    });
  };

  // Auth.isLoggedIn();
  authFactory.isLoggedIn = function() {
    if (AuthToken.getToken()) {
      return true;
    } else {
      return false;
    };
  };

  // Auth.isAdmin();
  authFactory.isAdmin = function() {
    return true;
  };

  // Auth.social(token);
  authFactory.social = function(token) {
    AuthToken.setToken(token);
  };

  // Auth.getUser();
  authFactory.getUser = function() {
    if (AuthToken.getToken()) {
      return $http.post('/api/me');
    } else {
      $q.reject({ message: "User has no token" });
    }
  };

  // Auth.logout();
  authFactory.logout = function() {
    AuthToken.setToken();
  };

  return authFactory;
})

.factory('AuthToken', function($window) {
  var authtokenFactory = {};

  // AuthToken.setToken(token);
  authtokenFactory.setToken = function(token) {
    if (token) {
      $window.localStorage.setItem('token', token);
    } else {
      $window.localStorage.removeItem('token');
    }
  };

  // AuthToken.getToken();
  authtokenFactory.getToken = function() {
    return $window.localStorage.getItem('token');
  };

  return authtokenFactory;
})

.factory('AuthInterceptors', function(AuthToken) {
  var authInterceptorsFactory = {};

  authInterceptorsFactory.request = function(config) {
    var token = AuthToken.getToken();

    if (token) {
      config.headers['x-access-token'] = token;
    }

    return config;
  };

  return authInterceptorsFactory;
});
