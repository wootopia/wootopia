angular.module('codeControllers', ['codeServices'])

.controller('codeController', function($http, $timeout, $location, $filter, Code) {

  var app = this;

  app.submitCode = function(codeData, valid, userdata) {
    app.codeData.kb_model = "Wooting One";
    app.codeData.game = app.selectedGame.id;

    var userDataAPI = {};
    userDataAPI.userdata = userdata;
    userDataAPI.newcode = app.codeData.code;
    app.loading = true;
    app.errMsg = false;

    if (valid) {

      // Check if code is in Wooting DB
      Code.getWootingObj(app.codeData.code).then(function(response) {
        console.log(response.data.rgb);
        if (response.data.rgb) {
          app.codeData.rgb = response.data.rgb;
          console.log('set var');

          Code.checkGame(app.selectedGame.id).then(function(data) {

            console.log('continuing mission');
            if (data.data) {
              // Append code to game in MongoDB
              app.selectedGame.code = app.codeData.code;
              app.selectedGame.code_notes = app.codeData.notes;
              app.selectedGame.code_rgb = app.codeData.rgb;
              Code.appendCode(app.selectedGame);
            } else {
              // Create a new game in MongoDB with this code
              app.selectedGame.code = app.codeData.code;
              app.selectedGame.code_notes = app.codeData.notes;
              app.selectedGame.code_rgb = app.codeData.rgb;
              app.selectedGame.hasPublishedCode = false;
              Code.createGame(app.selectedGame);
            }
          });

        } else {
          app.errMsg = "Code could not be found in the Wooting Database.";
        }
      });

      Code.create(app.codeData).then(function(data) {
        app.loading = false;
        if (data.data.success == true) {
          app.successMsg = data.data.message + ' ....Redirecting';
          $location.path('/').search({ submitted: true });
          var data = { 'game': app.selectedGame.name, 'code': app.codeData.code, 'notes': app.codeData.notes };
          Code.sendMailToAdmin(data);
        } else {
          app.errMsg = data.data.message;
        }
      });

    } else {
      app.loading = false;
      app.errMsg = 'Please ensure you filled out the code submission form correctly.';
    }

  }

  app.search = function(query) {
    app.selectedGame = null;
    Code.queryIgdb(query).then(function(data) {
      app.queryResults = data.data;
      angular.forEach(app.queryResults, function(value, index){
        Code.getGameDetails(value.id).then(function(details) {
          if (details.data[0].first_release_date) {
            value.details = details.data[0];
          }
        });
      })
    });
  }

  app.selectGame = function(id) {
    var found = $filter('getById')(app.queryResults, id);
    app.selectedGame = found.details;
  }

})

.controller("codeListController", function($http, games, Code, $routeParams, $location) {
  var app = this;

  app.games = games.data;

  if ($routeParams.submitted == true) {
    app.submitted = true;
  }

  app.submitClose = function() {
    $location.path('/').search({ submitted: false });
  };

  app.publishCode = function(gameId, codeId) {
    console.log('publish');
    var data = {};
    data.game_id = gameId;
    data.code_id = codeId;
    data.action = "submit"
    Code.updateCode(data);
  }

  app.rejectCode = function(gameId, codeId) {
    console.log('publish');
    var data = {};
    data.game_id = gameId;
    data.code_id = codeId;
    Code.updateCode(data);
  }

  app.stillUnpublished = function(gameId, codeId) {
    Code.getGame(gameId).then(function(data) {
      if (data.data) {
        return true;
      } else {
        return false;
      }
    });
  }

  app.generatePreview = function(code) {
    Code.getWootingObj(code).then(function(data) {
      for (var i = 0; i < data.data.rgb.kbdArray.length; i++) {
        for (var j = 0; j < data.data.rgb.kbdArray[i].length; j++) {
          // console.log(data.data.rgb.kbdArray[i][j]);
        }
      }
      // console.log(data.data.rgb.kbdArray);
    });
  }

});
