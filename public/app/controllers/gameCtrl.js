angular.module('gameControllers', ['codeServices'])

.controller('gameCtrl', function($routeParams, $http, $timeout, $location, $filter, Code) {

  var app = this;

  Code.getGame($routeParams.id).then(function(res) {
    app.game = res.data;
  });

});
